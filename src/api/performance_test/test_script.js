import request from '@/utils/request'

export function create(data) {
  return request({
    url: '/pt/testScript/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/pt/testScript/update',
    method: 'post',
    data
  })
}

export function selectById(id) {
  return request({
    url: '/pt/testScript/selectById',
    method: 'get',
    params: { id }
  })
}

export function testScriptDel(id) {
  return request({
    url: '/pt/testScript/delete',
    method: 'get',
    params: { id }
  })
}

export function selectByCatalogId(projectId, catalogId) {
  return request({
    url: '/pt/testScript/selectByCatalogId',
    method: 'get',
    params: { projectId, catalogId }
  })
}

export function search(data) {
  return request({
    url: '/pt/testScript/search',
    method: 'post',
    data
  })
}
