import request from '@/utils/request'

// 分页查询性能测试结果列表
export function perfTestResultList(data) {
  return request({
    url: '/pt/perfTestResult/list',
    method: 'post',
    data
  })
}

// 根据 id 查询 性能测试结果信息
export function getPerfTestResultInfoById(projectId, id) {
  return request({
    url: '/pt/perfTestResult/getPerfTestResultInfoById',
    method: 'get',
    params: { projectId, id }
  })
}

// 查询测试结果中的某个测试用例结果
export function getCaseResultDetails(resultId, testScriptId) {
  return request({
    url: '/pt/perfTestResult/getCaseResultDetails',
    method: 'get',
    params: { resultId, testScriptId }
  })
}

// 查询测试结果 - 测试报告
export function perfTestResultGetReport(resultId, testScriptId) {
  return request({
    url: '/pt/perfTestResult/getReport',
    method: 'get',
    params: { resultId, testScriptId }
  })
}

// 查询 性能测试实时图表页签 - 表格中显示的数据
export function perfTestResultGetLatestData(resultId, testScriptId) {
  return request({
    url: '/pt/perfTestResult/getLatestData',
    method: 'get',
    params: { resultId, testScriptId }
  })
}

// 查询 性能测试实时图表页签 - chart 绘图需要的数据
export function perfTestResultGetChart(resultId, testScriptId) {
  return request({
    url: '/pt/perfTestResult/getChart',
    method: 'get',
    params: { resultId, testScriptId }
  })
}
