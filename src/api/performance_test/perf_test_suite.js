import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/pt/perfTestSuite/list',
    method: 'post',
    data
  })
}

export function perfTestSuiteListAll(projectId) {
  return request({
    url: '/pt/perfTestSuite/listAll',
    method: 'get',
    params: { projectId }
  })
}

export function create(data) {
  return request({
    url: '/pt/perfTestSuite/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/pt/perfTestSuite/update',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/pt/perfTestSuite/delete',
    method: 'get',
    params: { id }
  })
}

export function findById(id) {
  return request({
    url: '/pt/perfTestSuite/findById',
    method: 'get',
    params: { id }
  })
}

export function run(projectId, id) {
  return request({
    url: '/pt/perfTestSuite/run',
    method: 'get',
    params: { projectId, id }
  })
}

export function perfForceStop(id) {
  return request({
    url: '/pt/perfTestSuite/forceStop',
    method: 'get',
    params: { id }
  })
}

export function perfUpdateBaseLine(id) {
  return request({
    url: '/pt/perfTestSuite/updateBaseLine',
    method: 'get',
    params: { id }
  })
}
