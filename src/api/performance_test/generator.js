import request from '@/utils/request'

export function listAll(projectId) {
  return request({
    url: '/pt/generator/listAll',
    method: 'get',
    params: { projectId }
  })
}

export function list(data) {
  return request({
    url: '/pt/generator/list',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/pt/generator/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/pt/generator/update',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/pt/generator/delete',
    method: 'get',
    params: { id }
  })
}
