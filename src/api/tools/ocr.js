import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/tools/ocr/list',
    method: 'post',
    data
  })
}

export function getText(data) {
  return request({
    url: '/tools/ocr/getText',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/tools/ocr/delete',
    method: 'get',
    params: { id }
  })
}
