import request from '@/utils/request'

export function fileUpload(data) {
  return request({
    url: '/tools/upload/fileUpload',
    method: 'post',
    data
  })
}

export function list(data) {
  return request({
    url: '/tools/upload/list',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/tools/upload/add',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/tools/upload/delete',
    method: 'get',
    params: { id }
  })
}

export function update(data) {
  return request({
    url: '/tools/upload/update',
    method: 'post',
    data
  })
}
