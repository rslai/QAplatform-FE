import request from '@/utils/request'

export function getTaskStatus(taskId) {
  return request({
    url: '/tools/taskManager/getTaskStatus',
    method: 'get',
    params: { taskId }
  })
}
