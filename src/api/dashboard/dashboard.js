import request from '@/utils/request'

export function getTotalCount(data) {
  return request({
    url: '/dashboard/getTotalCount',
    method: 'get',
    params: data
  })
}

export function getTestCaseCount(data) {
  return request({
    url: '/dashboard/getTestCaseCount',
    method: 'get',
    params: data
  })
}

