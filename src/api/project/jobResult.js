import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/project/jobResult/list',
    method: 'post',
    data
  })
}
