import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/project/job/list',
    method: 'post',
    data
  })
}

export function add(data) {
  return request({
    url: '/project/job/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: `/project/job/update`,
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/project/job/delete',
    method: 'get',
    params: { id }
  })
}

export function updateStatus(data) {
  return request({
    url: `/project/job/updateStatus`,
    method: 'post',
    data
  })
}
