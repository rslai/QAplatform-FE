import request from '@/utils/request'

export function listAll(projectId) {
  return request({
    url: '/project/robot/listAll',
    method: 'get',
    params: { projectId }
  })
}

export function add(data) {
  return request({
    url: '/project/robot/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: `/project/robot/update`,
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/project/robot/delete',
    method: 'get',
    params: { id }
  })
}
