import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/user/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/user/user/getInfo',
    method: 'get'
  })
}

export function selectWholeInfoById(data) {
  return request({
    url: '/user/user/selectWholeInfoById',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/user/user/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/user/user/update',
    method: 'post',
    data
  })
}

export function updateCurrentCompanyProject(data) {
  return request({
    url: '/user/user/updateCurrentCompanyProject',
    method: 'post',
    data
  })
}

export function locking(data) {
  return request({
    url: '/user/user/locking',
    method: 'post',
    data
  })
}

export function changeCode(data) {
  return request({
    url: '/user/user/changeCode',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: '/user/user/logout',
    method: 'post'
  })
}

export function list(data) {
  return request({
    url: '/user/user/list',
    method: 'post',
    data
  })
}

export function listByProjectId(data) {
  return request({
    url: '/user/user/listByProjectId',
    method: 'post',
    data
  })
}

export function selectUserByProjectId(id) {
  return request({
    url: '/user/user/selectUserByProjectId',
    method: 'get',
    params: { id }
  })
}

export function selectUserByNotIncludeProjectId(projectId) {
  return request({
    url: '/user/user/selectUserByNotIncludeProjectId',
    method: 'get',
    params: { projectId }
  })
}

export function generateToken(day) {
  return request({
    url: '/user/user/generateToken',
    method: 'get',
    params: { day }
  })
}

export function removeUserProject(userId, projectId) {
  return request({
    url: '/user/user/removeUserProject',
    method: 'get',
    params: { userId, projectId }
  })
}
