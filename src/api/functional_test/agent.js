import request from '@/utils/request'

export function selectAll(data) {
  return request({
    url: '/ft/agent/listAll',
    method: 'get'
  })
}

export function list(data) {
  return request({
    url: '/ft/agent/list',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/ft/agent/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/ft/agent/update',
    method: 'post',
    data
  })
}

export function reboot(key) {
  return request({
    url: '/ft/agent/reboot',
    method: 'get',
    params: { key }
  })
}
