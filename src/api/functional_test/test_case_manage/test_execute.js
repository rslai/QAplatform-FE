import request from '@/utils/request'

export function executePlan(data) {
  return request({
    url: '/ft/testExecute/updateCaseStatus',
    method: 'post',
    data
  })
}

export function listTree(data) {
  return request({
    url: '/ft/testExecute/listTree',
    method: 'post',
    data
  })
}
