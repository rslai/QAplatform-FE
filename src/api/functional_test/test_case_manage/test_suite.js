import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/ft/testSuite/list',
    method: 'post',
    data
  })
}

export function selectAll(data) {
  return request({
    url: '/ft/testSuite/selectAll',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/ft/testSuite/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/ft/testSuite/update',
    method: 'post',
    data
  })
}

export function findByID(id) {
  return request({
    url: '/ft/testSuite/findById',
    method: 'get',
    params: { id }
  })
}

export function del(id) {
  return request({
    url: '/ft/testSuite/delete',
    method: 'get',
    params: { id }
  })
}

export function runSuite(id) {
  return request({
    url: '/ft/testSuite/runSuite',
    method: 'get',
    params: { id }
  })
}

export function forceStopSuite(id) {
  return request({
    url: '/ft/testSuite/forceStopSuite',
    method: 'get',
    params: { id }
  })
}

export function funcUpdateReserved(id) {
  return request({
    url: '/ft/testSuite/updateReserved',
    method: 'get',
    params: { id }
  })
}
