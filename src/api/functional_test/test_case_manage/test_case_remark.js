import request from '@/utils/request'

export function create(data) {
  return request({
    url: '/ft/remark/add',
    method: 'post',
    data
  })
}

export function getCaseRemark(sid, cid) {
  return request({
    url: '/ft/remark/getCaseRemark',
    method: 'get',
    params: { sid, cid }
  })
}
