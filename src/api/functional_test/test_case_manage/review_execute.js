import request from '@/utils/request'

export function executePlan(data) {
  return request({
    url: '/ft/reviewExecute/updateCaseStatus',
    method: 'post',
    data
  })
}

// 批量更新测试用例编写状态
export function batchUpdateCaseStatus(data) {
  return request({
    url: '/ft/reviewExecute/batchUpdateCaseStatus',
    method: 'post',
    data
  })
}

export function listTree(data) {
  return request({
    url: '/ft/reviewExecute/listTree',
    method: 'post',
    data
  })
}
