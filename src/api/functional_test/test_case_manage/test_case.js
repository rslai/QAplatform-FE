import request from '@/utils/request'

export function create(data) {
  return request({
    url: '/ft/testCase/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/ft/testCase/update',
    method: 'post',
    data
  })
}

export function fileImport(filePath, projectId) {
  return request({
    url: '/ft/testCase/fileImport',
    method: 'get',
    params: { filePath, projectId }
  })
}

export function fileExport(data) {
  return request({
    url: '/ft/testCase/export',
    method: 'POST',
    data
  })
}

export function getManualTemplate() {
  return request({
    url: '/ft/testCase/getManualTemplate',
    method: 'get',
    responseType: 'blob'
  })
}

export function selectById(id) {
  return request({
    url: '/ft/testCase/selectById',
    method: 'get',
    params: { id }
  })
}

export function del(id) {
  return request({
    url: '/ft/testCase/delete',
    method: 'get',
    params: { id }
  })
}

export function selectByCatalogId(projectId, catalogId) {
  return request({
    url: '/ft/testCase/selectByCatalogId',
    method: 'get',
    params: { projectId, catalogId }
  })
}

export function saveOrUpdateBatch(data) {
  return request({
    url: '/ft/testCase/saveOrUpdateBatch',
    method: 'post',
    data
  })
}

export function search(data) {
  return request({
    url: '/ft/testCase/search',
    method: 'post',
    data
  })
}

export function testCaseDebug(data) {
  return request({
    url: '/ft/testCase/testCaseDebug',
    method: 'post',
    data
  })
}
