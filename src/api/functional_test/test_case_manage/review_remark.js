import request from '@/utils/request'

export function create(data) {
  return request({
    url: '/ft/reviewRemark/add',
    method: 'post',
    data
  })
}

// 批量添加评审备注
export function batchAddReviewRemark(data) {
  return request({
    url: '/ft/reviewRemark/batchAdd',
    method: 'post',
    data
  })
}

export function getCaseRemark(reviewsId, cid) {
  return request({
    url: '/ft/reviewRemark/getReviewRemark',
    method: 'get',
    params: { reviewsId, cid }
  })
}
