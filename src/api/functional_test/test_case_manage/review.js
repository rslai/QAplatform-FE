import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/ft/reviews/list',
    method: 'post',
    data
  })
}

export function selectAll(data) {
  return request({
    url: '/ft/testSuite/selectAll',
    method: 'post',
    data
  })
}

export function add(data) {
  return request({
    url: '/ft/reviews/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/ft/reviews/update',
    method: 'post',
    data
  })
}

export function findById(id) {
  return request({
    url: '/ft/reviews/findById',
    method: 'get',
    params: { id }
  })
}

export function del(id) {
  return request({
    url: '/ft/reviews/delete',
    method: 'get',
    params: { id }
  })
}

export function stop(id) {
  return request({
    url: '/ft/reviews/stop',
    method: 'get',
    params: { id }
  })
}
