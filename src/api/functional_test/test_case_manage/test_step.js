import request from '@/utils/request'

export function deleteByStepId(id) {
  return request({
    url: '/ft/step/deleteStepById',
    method: 'get',
    params: { id }
  })
}
