import request from '@/utils/request'

export function selectByTestCaseId(data) {
  return request({
    url: '/ft/testCaseHistory/selectByTestCaseId',
    method: 'get',
    params: data
  })
}
