import request from '@/utils/request'

export function listTreeContent(data) {
  return request({
    url: '/ft/catalog/listTreeContent',
    method: 'post',
    data
  })
}

export function listTree(data) {
  return request({
    url: '/ft/catalog/listTree',
    method: 'post',
    data
  })
}

export function listTreeCatalog(data) {
  return request({
    url: '/ft/catalog/listTreeCatalog',
    method: 'post',
    data
  })
}

/**
 * 调用根据当前元素id，递归查找控件元素以及父分类列表
 * @param type 节点类型（根据这个类型好读 case 表或 api 表）
 * @param id 元素id
 */
export function getElementParent(data) {
  return request({
    url: '/ft/catalog/getElementParent',
    method: 'post',
    data
  })
}

/**
 * tree 排序
 * @param type 节点类型（根据这个类型好读 case 表或 api 表）
 * @param source 源 node 数据
 * @param target 目标 node 数据
 * @param sortType 放置位置（before: 目标节点前、after：目标节点后、inner：目标节点内-子节点）
 * @returns {*}
 */
export function treeSort(projectId, type, source, target, sortType) {
  const data = {
    projectId: projectId,
    type: type,
    source: source,
    target: target,
    sortType: sortType === 'before' ? 1 : sortType === 'after' ? 2 : 3
  }
  return request({
    url: '/ft/catalog/treeSort',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/ft/catalog/add',
    method: 'post',
    data
  })
}

export function del(projectId, type, id) {
  return request({
    url: '/ft/catalog/delete',
    method: 'get',
    params: { projectId, type, id }
  })
}

export function update(data) {
  return request({
    url: '/ft/catalog/update',
    method: 'post',
    data
  })
}

export function selectById(id) {
  return request({
    url: '/ft/catalog/selectById',
    method: 'get',
    params: { id }
  })
}

export function copy(type, projectId, id) {
  return request({
    url: '/ft/catalog/copy',
    method: 'get',
    params: { type, projectId, id }
  })
}
