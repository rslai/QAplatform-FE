import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/ft/testResult/list',
    method: 'post',
    data
  })
}

export function getResultInfoById(projectId, id, type) {
  return request({
    url: '/ft/testResult/getResultInfoById',
    method: 'get',
    params: { projectId, id, type }
  })
}

export function getCaseResultDetails(cid, rid) {
  return request({
    url: '/ft/testResult/getCaseResultDetails',
    method: 'get',
    params: { cid, rid }
  })
}

export function getCaseResult(cids, sid) {
  const data = {
    cid: cids,
    sid: sid
  }
  return request({
    url: '/ft/testResult/getCaseResult',
    method: 'post',
    data
  })
}
