import request from '@/utils/request'

export function addEnv(data) {
  return request({
    url: 'ft/envParam/addEnv',
    method: 'post',
    data
  })
}

export function addServer(data) {
  return request({
    url: 'ft/envParam/addServer',
    method: 'post',
    data
  })
}

export function findList(data) {
  return request({
    url: 'ft/envParam/findEnvListParamsByName',
    method: 'post',
    data
  })
}
export function selectIdByEnvName(projectId, name) {
  return request({
    url: 'ft/envParam/selectIdByEnvName',
    method: 'get',
    params: { projectId, name }
  })
}
export function deleteEnvByName(projectId, envName) {
  return request({
    url: 'ft/envParam/deleteEnvByName',
    method: 'get',
    params: { projectId, envName }
  })
}

export function deleteServerById(id) {
  return request({
    url: 'ft/envParam/deleteServerById',
    method: 'get',
    params: { id }
  })
}
