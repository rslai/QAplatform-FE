import request from '@/utils/request'

export function selectByApiId(data) {
  return request({
    url: '/ft/apiHistory/selectByApiId',
    method: 'get',
    params: data
  })
}
