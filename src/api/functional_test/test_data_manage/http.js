import request from '@/utils/request'

export function del(type, id) {
  return request({
    url: '/catalog/catalog/delete',
    method: 'get',
    params: { type, id }
  })
}
