import request from '@/utils/request'

export function deleteById(id) {
  return request({
    url: '/ft/apiParams/deleteById',
    method: 'get',
    params: { id }
  })
}
