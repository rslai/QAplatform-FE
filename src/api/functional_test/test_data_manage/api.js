import request from '@/utils/request'

export function create(data) {
  return request({
    url: '/ft/api/add',
    method: 'post',
    data
  })
}

export function del(type, id) {
  return request({
    url: '/ft/api/delete',
    method: 'get',
    params: { type, id }
  })
}

export function selectById(type, id) {
  return request({
    url: '/ft/api/selectById',
    method: 'get',
    params: { type, id }
  })
}

export function update(data) {
  return request({
    url: '/ft/api/update',
    method: 'post',
    data
  })
}

export function search(data) {
  return request({
    url: '/ft/api/search',
    method: 'post',
    data
  })
}

export function apiFileImport(fileType, filePath, projectId) {
  return request({
    url: '/ft/api/apiFileImport',
    method: 'get',
    params: { fileType, filePath, projectId }
  })
}

export function apiCurlImport(data) {
  return request({
    url: '/ft/api/apiCurlImport',
    method: 'post',
    data
  })
}

export function apiExport(data) {
  return request({
    url: '/ft/api/apiExport',
    method: 'post',
    data
  })
}

export function apiDebug(data) {
  return request({
    url: '/ft/api/apiDebug',
    method: 'post',
    data
  })
}

export function getApiDebugStatus(sessionId, timestamp) {
  return request({
    url: '/ft/api/getApiDebugStatus',
    method: 'get',
    params: { sessionId, timestamp }
  })
}
