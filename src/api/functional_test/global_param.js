import request from '@/utils/request'

export function create(data) {
  return request({
    url: '/ft/globalParam/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/ft/globalParam/update',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/ft/globalParam/deleteById',
    method: 'get',
    params: { id }
  })
}

export function list(projectId) {
  return request({
    url: '/ft/globalParam/selectAll',
    method: 'get',
    params: { projectId }
  })
}

