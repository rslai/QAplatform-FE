import request from '@/utils/request'

export function selectAll(data) {
  return request({
    url: '/manage/company/selectAll',
    method: 'get'
  })
}

export function list(data) {
  return request({
    url: '/manage/company/list',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/manage/company/add',
    method: 'post',
    data
  })
}

export function getUserCompanyInfo() {
  return request({
    url: '/manage/company/getUserCompanyInfo',
    method: 'get'
  })
}

export function update(data) {
  return request({
    url: '/manage/company/update',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/manage/company/delete',
    method: 'get',
    params: { id }
  })
}

