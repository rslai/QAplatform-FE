import request from '@/utils/request'

export function selectAll(data) {
  return request({
    url: '/manage/project/selectAll',
    method: 'get'
  })
}

export function list(data) {
  return request({
    url: '/manage/project/list',
    method: 'post',
    data
  })
}

export function selectById(data) {
  return request({
    url: '/manage/project/selectById',
    method: 'post',
    data
  })
}

export function create(data) {
  return request({
    url: '/manage/project/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/manage/project/update',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/manage/project/delete',
    method: 'get',
    params: { id }
  })
}

