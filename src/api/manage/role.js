import request from '@/utils/request'

// 得到所有权限（Route 菜单 + Permission 按钮）
export function getPermission() {
  return request({
    url: '/manage/role/getPermission',
    method: 'get'
  })
}

// 得到角色的所有权限（Route 菜单 + Permission 按钮）
export function getRolePermission(id) {
  return request({
    url: '/manage/role/getRolePermission',
    method: 'get',
    params: { id }
  })
}

// 保存角色的所有权限（Route 菜单 + Permission 按钮）
export function saveRolePermission(data) {
  return request({
    url: '/manage/role/saveRolePermission',
    method: 'post',
    data
  })
}

export function list(data) {
  return request({
    url: '/manage/role/list',
    method: 'post',
    data
  })
}

export function listAll() {
  return request({
    url: '/manage/role/listAll',
    method: 'get'
  })
}

export function listAllProjectRoles() {
  return request({
    url: '/manage/role/listAllProjectRoles',
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: '/manage/role/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: `/manage/role/update`,
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: '/manage/role/delete',
    method: 'get',
    params: { id }
  })
}
