import request from '@/utils/request'

export function listTree() {
  return request({
    url: 'manage/route/listTree',
    method: 'get'
  })
}

export function selectById(id) {
  return request({
    url: 'manage/route/selectById',
    method: 'get',
    params: { id }
  })
}

export function add(data) {
  return request({
    url: 'manage/route/add',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: `manage/route/update`,
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'manage/route/delete',
    method: 'get',
    params: { id }
  })
}
