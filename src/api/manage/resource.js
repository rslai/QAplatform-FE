import request from '@/utils/request'

export function list(data) {
  return request({
    url: '/manage/resource/list',
    method: 'post',
    data
  })
}

export function listAll() {
  return request({
    url: '/manage/resource/listAll',
    method: 'get'
  })
}

export function del(id) {
  return request({
    url: '/manage/resource/delete',
    method: 'get',
    params: { id }
  })
}

export function syncResource() {
  return request({
    url: '/manage/resource/sync',
    method: 'get'
  })
}
