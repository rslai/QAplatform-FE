/*
 * generate-schema 库增强工具
 */
var jsonpathPlus = require('jsonpath-plus')

/**
 * 递归设置默认值
 * @param properties properties 对象
 * @param parentXpath 父级 xpath（当前节点 xpah 会加此下）
 * @param json json 对象
 */
function recursiveSetDefaultValue(properties, parentXpath, json) {
  // console.log('parentXpath:', parentXpath)
  if (!properties) {
    return
  }

  Object.entries(properties).forEach(([key, value]) => {
    const xpath = parentXpath + '.' + key

    // console.log('key:', key)
    // console.log('value:', value)
    // console.log('xpath:', xpath)

    if (value.type === 'object') {
      return recursiveSetDefaultValue(value.properties, xpath, json)
    } else if (value.type === 'array') {
      if (value.items.type === 'object') {
        return recursiveSetDefaultValue(value.items.properties, xpath + '[0]', json)
      } else {
        const df = jsonpathPlus.JSONPath({ path: xpath, json: json })[0]
        if (df.length > 0) {
          value.default = df
        }
      }
    } else {
      const df = jsonpathPlus.JSONPath({ path: xpath, json: json })[0]
      value.default = df
    }
  })
}

/**
 * 设置 schema 的默认值
 * @param json json 对象
 * @param schema schema 对象
 * @returns {*}
 */
export function setSchemaDefaultValue(json, schema) {
  // console.log('schema:', schema)
  const xpath = '$'
  recursiveSetDefaultValue(schema.properties, xpath, json)
  return schema
}

