/**
 * 用例编辑状态 枚举类
 */
export const editStatusOptions = [
  {
    value: 1,
    name: '编写中',
    disabled: false
  }, {
    value: 2,
    name: '等待评审',
    disabled: false
  }, {
    value: 3,
    name: '评审不通过',
    disabled: false
  }, {
    value: 4,
    name: '评审通过',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getEditStatusValue(name) {
  for (const item of editStatusOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getEditStatusName(value) {
  for (const item of editStatusOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
