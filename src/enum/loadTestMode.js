/**
 * 压测模式 枚举类
 */
export const loadTestModeOptions = [
  {
    value: 1,
    name: '并发模式',
    disabled: false
  }, {
    value: 2,
    name: '轮次模式',
    disabled: true
  }, {
    value: 3,
    name: '阶梯模式',
    disabled: false
  }, {
    value: 4,
    name: '错误率模式',
    disabled: true
  }, {
    value: 5,
    name: '响应时间模式',
    disabled: true
  }, {
    value: 6,
    name: '每秒应答数模式',
    disabled: true
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getLoadTestModeValue(name) {
  for (const item of loadTestModeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getLoadTestModeName(value) {
  for (const item of loadTestModeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
