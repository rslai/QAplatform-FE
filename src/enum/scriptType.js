/**
 * 性能测试脚本类型 枚举类
 */
export const scriptTypeOptions = [
  {
    value: 1,
    name: 'Locust',
    disabled: false
  }, {
    value: 2,
    name: 'JMeter',
    disabled: true
  }, {
    value: 3,
    name: 'LoadRunner',
    disabled: true
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getScriptTypeValue(name) {
  for (const item of scriptTypeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getScriptTypeName(value) {
  for (const item of scriptTypeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
