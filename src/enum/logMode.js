/**
 * 日志模式 枚举类
 */
export const logModeOptions = [
  {
    value: 1,
    name: '关闭',
    disabled: false
  }, {
    value: 2,
    name: '开启-全部日志',
    disabled: false
  }, {
    value: 3,
    name: '开启-仅成功日志',
    disabled: false
  }, {
    value: 4,
    name: '开启-仅错误日志',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getLogModeValue(name) {
  for (const item of logModeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getLogModeName(value) {
  for (const item of logModeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
