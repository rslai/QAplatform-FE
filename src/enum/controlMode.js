/**
 * 控制模式 枚举类
 */
export const controlModeOptions = [
  {
    value: 1,
    name: '集中模式', // 集中模式（同时启动所有并发（设置的并发数/线程/协程），当设置的并发数全部结束后（某个线程（协程）完成后需要等待其他的线程（协程）完成），再次启动所设置的并发进行施压。）
    disabled: false
  }, {
    value: 2,
    name: '单独模式', // 单独模式（同时启动所有并发（设置的并发数/线程/协程），当其中的某个或某些线程（协程）完成后，立即再次启动完成的线程（协程），不等待其他的线程（协程）。）
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getControlModeValue(name) {
  for (const item of controlModeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getControlModeName(value) {
  for (const item of controlModeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
