/**
 * 执行任务状态 枚举类
 */
export const taskStatusOptions = [
  {
    value: 0,
    name: 'none',
    disabled: false
  }, {
    value: 1,
    name: '创建',
    disabled: false
  }, {
    value: 2,
    name: '运行中',
    disabled: false
  }, {
    value: 3,
    name: '结束-中断',
    disabled: false
  }, {
    value: 4,
    name: '结束-失败',
    disabled: false
  }, {
    value: 5,
    name: '结束-成功',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getTaskStatusValue(name) {
  for (const item of taskStatusOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getTaskStatusName(value) {
  for (const item of taskStatusOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
