/**
 * 分类类型 枚举类
 */
export const catalogTypeOptions = [
  {
    value: 0,
    name: 'none',
    disabled: false
  }, {
    value: 1,
    name: '测试用例',
    disabled: false
  }, {
    value: 3,
    name: '接口测试用例',
    disabled: false
  }, {
    value: 4,
    name: '公共步骤',
    disabled: false
  }, {
    value: 2,
    name: '测试套件',
    disabled: false
  }, {
    value: 5,
    name: '性能测试用例',
    disabled: false
  }, {
    value: 6,
    name: '性能测试套件',
    disabled: false
  }, {
    value: 11,
    name: 'http 接口',
    disabled: false
  }, {
    value: 12,
    name: 'WebSocket 接口',
    disabled: false
  }, {
    value: 13,
    name: 'DataChannel 接口',
    disabled: false
  }, {
    value: 14,
    name: 'Database 接口',
    disabled: false
  }, {
    value: 15,
    name: 'SSH 接口',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getCatalogTypeValue(name) {
  for (const item of catalogTypeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getCatalogTypeName(value) {
  for (const item of catalogTypeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
