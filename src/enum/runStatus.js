/**
 * 执行状态 枚举类
 */
export const runStatusOptions = [
  {
    value: 1,
    name: '待运行',
    color: '#7cb9fa',
    elTagType: 'info',
    disabled: false
  }, {
    value: 2,
    name: '运行中',
    color: '#ffa500',
    elTagType: 'info',
    disabled: false
  }, {
    value: 3,
    name: '信息',
    color: '#7cb9fa',
    elTagType: 'info',
    disabled: false
  }, {
    value: 4,
    name: '通过',
    color: '#03f904',
    elTagType: 'success',
    disabled: false
  }, {
    value: 5,
    name: '警告',
    color: '#f4ca0c',
    elTagType: 'warning',
    disabled: false
  }, {
    value: 11,
    name: '跳过',
    color: '#9a9791',
    elTagType: 'info',
    disabled: false
  }, {
    value: 12,
    name: '中断',
    color: '#f22d0f',
    elTagType: 'warning',
    disabled: false
  }, {
    value: 13,
    name: '未知',
    color: '#9a9791',
    elTagType: 'warning',
    disabled: false
  }, {
    value: 21,
    name: '错误',
    color: '#ef1636',
    elTagType: 'danger',
    disabled: false
  }, {
    value: 22,
    name: '失败',
    color: '#ef1636',
    elTagType: 'danger',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getRunStatusValue(name) {
  for (const item of runStatusOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getRunStatusName(value) {
  for (const item of runStatusOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 color
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getRunStatusColor(value) {
  for (const item of runStatusOptions) {
    if (item.value === value) {
      return item.color
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举在 el-tag 中的 type 属性
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getRunStatusElTagType(value) {
  for (const item of runStatusOptions) {
    if (item.value === value) {
      return item.elTagType
    }
  }
  return 'undefined'
}
