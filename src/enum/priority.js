/**
 * 用例优先级 枚举类
 */
export const priorityOptions = [
  {
    value: 0,
    name: 'P0',
    disabled: false
  }, {
    value: 1,
    name: 'P1',
    disabled: false
  }, {
    value: 2,
    name: 'P2',
    disabled: false
  }, {
    value: 3,
    name: 'P3',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getPriorityValue(name) {
  for (const item of priorityOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getPriorityName(value) {
  for (const item of priorityOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
