/**
 * 条件 枚举类
 */
export const conditionTypeOptions = [
  {
    value: 0,
    name: '无',
    disabled: false
  }, {
    value: 1,
    name: 'if',
    disabled: false
  }, {
    value: 2,
    name: 'else if',
    disabled: false
  }, {
    value: 3,
    name: 'else',
    disabled: false
  }, {
    value: 4,
    name: 'while',
    disabled: false
  }, {
    value: 5,
    name: 'for',
    disabled: false
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getConditionTypeValue(name) {
  for (const item of conditionTypeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getConditionTypeName(value) {
  for (const item of conditionTypeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
