/**
 * 操作系统类型 枚举类
 */
export const systemTypeOptions = [
  {
    value: 1,
    name: 'Linux',
    disabled: false
  }, {
    value: 2,
    name: 'Windows',
    disabled: true
  }, {
    value: 3,
    name: 'macOS',
    disabled: true
  }
]

/**
 * 根据 枚举的 name 获取 枚举的 value
 * @param name 枚举的 name
 * @returns {number|*|string}
 */
export function getSystemTypeValue(name) {
  for (const item of systemTypeOptions) {
    if (item.name === name) {
      return item.value
    }
  }
  return 'undefined'
}

/**
 * 根据 枚举的 value 获取 枚举的 name
 * @param value 枚举的 value
 * @returns {string|string|*}
 */
export function getSystemTypeName(value) {
  for (const item of systemTypeOptions) {
    if (item.value === value) {
      return item.name
    }
  }
  return 'undefined'
}
