import Layout from '@/layout'

const performanceTestRouter = {
  component: Layout,
  path: '/performance_test',
  name: 'performance_test',
  meta: { title: '性能测试', icon: 'el-icon-s-unfold' },
  redirect: '/performance_test/performance',
  alwaysShow: true,
  children: [
    {
      hidden: true,
      path: 'performance',
      name: 'performance',
      meta: { title: '敬请期待' }
      // component: () => import('@/views/performance_test/performance')
    }
  ]
}

export default performanceTestRouter
