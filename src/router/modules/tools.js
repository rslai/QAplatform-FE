/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const toolsRouter = {
  path: '/tools',
  component: Layout,
  redirect: '/tools/ocr',
  name: 'tools',
  meta: { title: '工具', icon: 'wrench' },
  alwaysShow: true,
  children: [
    {
      path: 'ocr',
      name: 'ocr',
      component: () => import('@/views/tools/ocr_list'),
      meta: { title: 'OCR 文字识别' }
    },
    {
      path: 'fileUpload',
      name: 'fileUpload',
      component: () => import('@/views/tools/upload_list'),
      meta: { title: '文件上传' }
    }
  ]
}
export default toolsRouter
