import Layout from '@/layout'

const functionalTestRouter = {
  component: Layout,
  path: '/functional_test',
  name: 'functional_test',
  meta: { title: '功能测试', icon: 'el-icon-data-line' },
  redirect: '/functional_test/test_case',
  children: [
    {
      path: 'test_case',
      name: 'test_case',
      component: () => import('@/views/functional_test/test_case_manage/test_case'),
      meta: { title: '测试用例' },
      props: { caseType: 1 }
    },
    {
      path: 'edit_testsuite',
      name: 'edit_testsuite',
      component: () => import('@/views/functional_test/test_case_manage/edit_testsuite'),
      meta: { title: '编辑套件', icon: 'form' },
      hidden: true
    },
    {
      path: 'edit_review',
      name: 'edit_review',
      component: () => import('@/views/functional_test/test_case_manage/edit_review'),
      meta: { title: '编辑评审', icon: 'form' },
      hidden: true
    },
    {
      path: 'review_execute',
      name: 'review_execute',
      component: () => import('@/views/functional_test/test_case_manage/review_execute'),
      meta: { title: '执行评审', icon: 'form' },
      hidden: true
    },
    {
      path: 'plan/execute',
      name: 'test_execute',
      component: () => import('@/views/functional_test/test_case_manage/test_execute'),
      meta: { title: '执行套件', icon: 'form' },
      hidden: true
    },
    {
      path: 'test_plan',
      name: 'test_plan',
      meta: { title: '测试执行' },
      component: () => import('@/views/functional_test/test_case_manage/test_plan'),
      props: { caseType: 1 }
    },
    {
      path: 'result_detail',
      name: 'result_detail',
      component: () => import('@/views/functional_test/automated_test/result_detail'),
      meta: { title: '报告详情', icon: 'form' },
      hidden: true
    },
    {
      path: 'review',
      name: 'review',
      meta: { title: '用例评审' },
      component: () => import('@/views/functional_test/test_case_manage/review')
    },
    {
      path: 'data_manage',
      name: 'data_manage',
      component: () => import('@/views/functional_test/test_case_manage/functional_test_case_import'),
      meta: { title: '用例导入' }
    }
  ]
}

export default functionalTestRouter
