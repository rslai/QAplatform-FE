import Layout from '@/layout'

const interfaceTestRouter = {
  component: Layout,
  path: '/interface_test',
  name: 'interface_test',
  meta: { title: '接口测试', icon: 'el-icon-data-line' },
  redirect: '/interface_test/test_case',
  children: [
    {
      path: 'test_case',
      name: 'test_case',
      linkActiveClass: 'router-active',
      component: () => import('@/views/functional_test/test_case_manage/test_case'),
      meta: { title: '测试用例' },
      props: { caseType: 3 }
    },
    {
      path: 'public_step',
      name: 'public_step',
      meta: { title: '公共步骤' },
      component: () => import('@/views/functional_test/test_case_manage/test_case'),
      props: { caseType: 4 }
    },
    {
      path: 'test_suite',
      name: 'test_suite',
      meta: { title: '测试套件' },
      component: () => import('@/views/functional_test/test_case_manage/test_suite'),
      props: { caseType: 2 }
    },
    {
      path: 'edit_testsuite',
      name: 'edit_testsuite',
      component: () => import('@/views/functional_test/test_case_manage/edit_testsuite'),
      meta: { title: '编辑套件', icon: 'form' },
      hidden: true
    },
    {
      path: 'test_results',
      name: 'test_results',
      meta: { title: '测试报告' },
      component: () => import('@/views/functional_test/automated_test/test_results')
    },
    {
      path: 'result_detail',
      name: 'result_detail',
      component: () => import('@/views/functional_test/automated_test/result_detail'),
      meta: { title: '报告详情', icon: 'form' },
      hidden: true
    },
    {
      path: 'interface_define',
      name: 'interface_define',
      redirect: '/interface_test/interface_define/http',
      meta: { title: '接口定义' },
      component: () => import('@/views/router_view'),
      children: [
        {
          path: 'http',
          name: 'http',
          meta: { title: 'HTTP' },
          component: () => import('@/views/functional_test/test_data_manage/interface_define/http')
        },
        {
          path: 'web_socket',
          name: 'web_socket',
          meta: { title: 'WebSocket' },
          component: () => import('@/views/functional_test/test_data_manage/interface_define/web_socket')
        },
        {
          path: 'data_channel',
          name: 'data_channel',
          meta: { title: 'DataChannel' },
          component: () => import('@/views/functional_test/test_data_manage/interface_define/data_channel')
        },
        {
          path: 'database',
          name: 'database',
          meta: { title: 'Database' },
          component: () => import('@/views/functional_test/test_data_manage/interface_define/database')
        },
        {
          path: 'ssh',
          name: 'ssh',
          meta: { title: 'SSH' },
          component: () => import('@/views/functional_test/test_data_manage/interface_define/ssh')
        }
      ]
    },
    {
      path: 'param_define',
      name: 'param_define',
      meta: { title: '参数定义' },
      component: () => import('@/views/router_view'),
      children: [
        {
          path: 'global_param',
          name: 'global_param',
          meta: { title: '全局参数' },
          component: () => import('@/views/functional_test/test_data_manage/global_param')
        },
        {
          path: 'env_param',
          name: 'env_param',
          meta: { title: '环境参数' },
          component: () => import('@/views/functional_test/test_data_manage/env_param')
        }
      ]
    },
    {
      path: 'jobs',
      name: 'jobs',
      meta: { title: '定时任务' },
      component: () => import('@/views/functional_test/automated_test/jobs')
    }
  ]
}

export default interfaceTestRouter
