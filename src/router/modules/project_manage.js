/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const projectManageRouter = {
  path: '/project_manage',
  component: Layout,
  redirect: '/project_manage/members',
  name: 'project_manage',
  meta: { title: '项目管理', icon: 'el-icon-set-up' },
  alwaysShow: true,
  children: [
    {
      path: 'members',
      name: 'members',
      component: () => import('@/views/manage/members'),
      meta: { title: '成员管理' }
    },
    {
      path: 'project_settings',
      name: 'project_settings',
      component: () => import('@/views/manage/project_settings'),
      meta: { title: '项目设置' }
    }
  ]
}
export default projectManageRouter
