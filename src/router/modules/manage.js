/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const manageRouter = {
  path: '/manage',
  component: Layout,
  redirect: '/manage/users',
  name: 'manage',
  meta: { title: '系统管理', icon: 'el-icon-s-tools' },
  alwaysShow: true,
  children: [
    {
      path: 'users',
      name: 'users',
      component: () => import('@/views/manage/users'),
      meta: { title: '用户管理' }
    },
    {
      path: '/manage/users/edit_user',
      component: () => import('@/views/manage/edit_user'),
      meta: { title: '编辑用户', icon: 'form' },
      hidden: true
    },
    {
      path: 'company_list',
      name: 'company_list',
      component: () => import('@/views/manage/company_list'),
      meta: { title: '公司管理' }
    },
    {
      path: 'project_list',
      name: 'project_list',
      component: () => import('@/views/manage/project_list'),
      meta: { title: '项目管理' }
    }
  ]
}
export default manageRouter
