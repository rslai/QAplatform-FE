import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'dashboard', affix: true, noCache: true }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  /** when your routing map is too long, you can split it into small modules **/

  // 404页必须放在末尾！！！ 要是放在静态路由里那么所有动态路由都不能通过输入地址访问到了
  { path: '*', redirect: '/404', hidden: true }
]

// 组件名 和 组件对象 映射表，用于后续根据后台传过来的 组件名 替换成 组件对象 使用
export const componentMap = {
  'layout': require('@/layout').default,
  // 功能测试
  'test_case': () => import('@/views/functional_test/test_case_manage/test_case').then(m => m.default),
  'edit_testsuite': () => import('@/views/functional_test/test_case_manage/edit_testsuite').then(m => m.default),
  'edit_review': () => import('@/views/functional_test/test_case_manage/edit_review').then(m => m.default),
  'review_execute': () => import('@/views/functional_test/test_case_manage/review_execute').then(m => m.default),
  'test_execute': () => import('@/views/functional_test/test_case_manage/test_execute').then(m => m.default),
  'test_plan': () => import('@/views/functional_test/test_case_manage/test_plan').then(m => m.default),
  'result_detail': () => import('@/views/functional_test/automated_test/result_detail').then(m => m.default),
  'review': () => import('@/views/functional_test/test_case_manage/review').then(m => m.default),
  'data_manage': () => import('@/views/functional_test/test_case_manage/functional_test_case_import').then(m => m.default),
  // 接口测试
  'test_suite': () => import('@/views/functional_test/test_case_manage/test_suite').then(m => m.default),
  'test_results': () => import('@/views/functional_test/automated_test/test_results').then(m => m.default),
  // 接口测试 - 接口定义
  'interface_define': () => import('@/views/router_view').then(m => m.default),
  'http': () => import('@/views/functional_test/test_data_manage/interface_define/http').then(m => m.default),
  'web_socket': () => import('@/views/functional_test/test_data_manage/interface_define/web_socket').then(m => m.default),
  'data_channel': () => import('@/views/functional_test/test_data_manage/interface_define/data_channel').then(m => m.default),
  'database': () => import('@/views/functional_test/test_data_manage/interface_define/database').then(m => m.default),
  'ssh': () => import('@/views/functional_test/test_data_manage/interface_define/ssh').then(m => m.default),
  // 接口测试 - 参数定义
  'param_define': () => import('@/views/router_view').then(m => m.default),
  'global_param': () => import('@/views/functional_test/test_data_manage/global_param').then(m => m.default),
  'env_param': () => import('@/views/functional_test/test_data_manage/env_param').then(m => m.default),
  // 接口测试 - 数据导入
  'interface_test_data_import': () => import('@/views/functional_test/test_data_manage/interface_test_data_import').then(m => m.default),
  // 性能测试
  'test_script': () => import('@/views/performance_test/test_script').then(m => m.default),
  'perf_test_suite': () => import('@/views/performance_test/perf_test_suite').then(m => m.default),
  'perf_result_detail': () => import('@/views/performance_test/perf_result_detail').then(m => m.default),
  'generator': () => import('@/views/performance_test/generator').then(m => m.default),
  // 工具
  'ocr': () => import('@/views/tools/ocr_list').then(m => m.default),
  'fileUpload': () => import('@/views/tools/upload_list').then(m => m.default),
  // 项目管理
  'members': () => import('@/views/project/members').then(m => m.default),
  'project_settings': () => import('@/views/project/project_settings').then(m => m.default),
  'job': () => import('@/views/project/job').then(m => m.default),
  'jobResult': () => import('@/views/project/jobResult').then(m => m.default),
  'editor_dashboard': () => import('@/views/dashboard/editor/index').then(m => m.default),
  'company_settings': () => import('@/views/manage/company_settings').then(m => m.default),
  // 系统管理
  'users': () => import('@/views/manage/users').then(m => m.default),
  'edit_user': () => import('@/views/manage/edit_user').then(m => m.default),
  'company_list': () => import('@/views/manage/company_list').then(m => m.default),
  'project_list': () => import('@/views/manage/project_list').then(m => m.default),
  'permission': () => import('@/views/router_view').then(m => m.default),
  'resource': () => import('@/views/manage/resource').then(m => m.default),
  'menu': () => import('@/views/manage/menu').then(m => m.default),
  'roles': () => import('@/views/manage/role').then(m => m.default),
  'sysJob': () => import('@/views/manage/sysJob').then(m => m.default),
  'sysJobResult': () => import('@/views/manage/sysJobResult').then(m => m.default),
  // Agent
  'agent': () => import('@/views/functional_test/agent').then(m => m.default)
}

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
