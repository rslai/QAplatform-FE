nohup google-chrome --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222 --no-sandbox --disable-dev-shm-usage --headless --disable-gpu  >/dev/null 2>&1 &
sed -i "s/SERVER_PROTOCOL/${SERVER_PROTOCOL}/g" /usr/share/nginx/html/static/js/*.js
sed -i "s/SERVER_HOST/${SERVER_HOST}/g" /usr/share/nginx/html/static/js/*.js
sed -i "s/SERVER_PORT/${SERVER_PORT}/g" /usr/share/nginx/html/static/js/*.js
/usr/sbin/nginx -c /etc/nginx/nginx.conf -g 'daemon off;'
