## 开发

```bash
# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:9527

## 发布测试环境

```bash
# 构建测试环境
npm run build:stage
```

## 发布生产环境

```bash
# 删除上一次的编译结果（不删除，前端页面需要强制刷新才能更新）
rm -rf dist/

# 构建生产环境
npm run build:prod

# 打包
docker build -t registry.cn-hangzhou.aliyuncs.com/netlrs/qa-platform-web:v1.0.1-SNAPSHOT .

# 推送
docker push registry.cn-hangzhou.aliyuncs.com/netlrs/qa-platform-web:v1.0.1-SNAPSHOT
```
